from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.events_list, name='events_list'),
    url(r'^event/(?P<pk>[0-9]+)/$', views.event_details, name='event_details'),
    url(r'^event/vote/(?P<pk>[0-9]+)/$', views.event_vote, name='event_vote'),
    url(r'^event/new/$', views.event_new, name='event_new'),
	url(r'^event/(?P<pk>[0-9]+)/edit/$', views.event_edit, name='event_edit'),
]

