from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.shortcuts import redirect
from .models import Event
from .forms import EventForm


def events_list(request):
	events = Event.objects.all()#
	Event.objects.filter(event_date__lte=timezone.now()).order_by('event_date')

	return render(request, 'event/events_list.html', {'events':events})

def event_details(request, pk):
	event = get_object_or_404(Event, pk=pk)
	return render(request, 'event/event_details.html', {'event': event})

def event_vote(request, pk):
	event = get_object_or_404(Event, pk=pk)
	event.votes += 1
	event.save()

	return redirect('event_details', pk=event.pk)
	# event = get_object_or_404(Event, pk=pk)
	# return render(request, 'event/event_details.html', {'event': event})

def event_new(request):
	if request.method == "POST":
		form = EventForm(request.POST)
		if form.is_valid():
			event = form.save(commit=False)
			event.author = request.user
			# post.published_date = timezone.now()
			event.save()
			return redirect('event_details', pk=event.pk)
	else:
		form = EventForm()
	return render(request, 'event/event_edit.html', {'form': form})

def event_edit(request, pk):
	event = get_object_or_404(Event, pk=pk)
	if request.method == "POST":
		form = EventForm(request.POST, instance=event)
		if form.is_valid():
			event = form.save(commit=False)
			event.author = request.user
			# event.publised_date = timezone.now()
			event.save()
			return redirect('event_details', pk=event.pk)
	else:
		form = EventForm(instance=event)
	return render(request, 'event/event_edit.html', {'form': form})



# Create your views here.
