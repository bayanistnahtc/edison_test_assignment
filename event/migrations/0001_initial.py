# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-07 07:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('describtion', models.TextField()),
                ('created_date', models.DateTimeField()),
                ('event_date', models.DateTimeField()),
                ('votes', models.IntegerField()),
            ],
            options={
                'db_table': 'event',
            },
        ),
    ]
