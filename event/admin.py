from django.contrib import admin
from event.models import Event

class EventAdmin(admin.ModelAdmin):
	fields = ['title', 'description', 'created_date', 'event_date']
	list_filter = ['created_date', 'event_date']

admin.site.register(Event, EventAdmin)