from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Event(models.Model):
	"""docstring for Event"""
	class Meta:
		db_table = "event"
	author = models.ForeignKey(User)
	title = models.CharField(max_length = 200)
	description = models.TextField(blank=True, null=True)
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(default=timezone.now)
	event_date = models.DateTimeField(default=timezone.now)
	votes = models.IntegerField(default=0)
	# status = ['historical', 'closed', 'first_stage', 'second_stage', 'future'] in the future dev
	status = models.BooleanField(default=True)

class Vote(models.Model):
	"""docstring for Vote"""
	class Meta:
		db_table = 'vote'
	author = models.ForeignKey(User)

		
