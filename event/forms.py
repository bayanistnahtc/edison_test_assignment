from django import forms
from .models import Event, Vote

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ('title', 'description', 'created_date', 'event_date')

class VoteForm(forms.ModelForm):
    class Meta:
        model = Vote
        fields = ('author',)


		